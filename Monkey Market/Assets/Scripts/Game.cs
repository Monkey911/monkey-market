﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Game : MonoBehaviour {
    public int TotalTime;
    public CanvasScript Canvas;
    public float SpawnTime;
    public float DisappearTime;
    public float MinimumDistance;
    public bool GameOver = false;
    public CustomerController CustomerPrefab;
    public int currentLevel;


    private static Game instance;
    public static float MarketMinimumXValue = -24;
    public static float MarketMaximumXValue = 114.7f;
    public static float MarketMinimumYValue = -8.4f;
    public static float MarketMaximumYValue = -4f;

    private AudioSource Audio;
    private float TimeLeft = 0;
    private bool isPaused = false;

    private Timer TimerScript;
    private Coin CoinScript;
    private Heart HeartScript;
    private CobraController CobraController;
    private int Budget;
    private int Health;
    private List<string> BoughtItems;

    private int SpawnedCobras = 0;
    private int SpawnedCobrasLimit;

    private LevelInformation LevelDataScript;

    public static Game Instance {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        LevelDataScript = GetComponent<LevelInformation>();
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    void Start () {
        InitializeComponents();
        InitializeValues();
        CallSpawningScripts();
    }

    private void InitializeValues()
    {
        instance = this;
        GameOver = false;
        TimeLeft = 0;
        Budget = 0;
        BoughtItems = new List<string>();
        Time.timeScale = 1;

        SpawnedCobrasLimit = (int)GetCurrentLevelData()["amountOfCobras"];
        if (currentLevel > 1)
        {
            Health = (int)GetCurrentLevelData()["health"];
            Canvas.UpdateHealthValue(Health);
        }


        GenerateNegotiableStands();

    }

    private void InitializeComponents()
    {
        Audio = GetComponent<AudioSource>();
        TimerScript = GetComponent<Timer>();
        CoinScript = GetComponent<Coin>();
        if (currentLevel > 1) HeartScript = GetComponent<Heart>();
        CobraController = GetComponent<CobraController>();
    }

    private void CallSpawningScripts()
    {
        InvokeRepeating("CallSpawningBuffs", SpawnTime, SpawnTime);
        InvokeRepeating("CallSpawningCobras", CobraController.spawnTime, CobraController.spawnTime);
        InvokeRepeating("CallDisappearingBuffs", DisappearTime, DisappearTime);

        SpawnCustomers();
    }

    public int GetHealth()
    {
        return Health;
    }

    public void IncreaseHealth(int amount)
    {
        Health += amount;
        Canvas.UpdateHealthValue(Health);
    }

    public void DecreaseHealth(int harm)
    {
        Health -= harm;
        if (Health <= 0) {
            GameOver = true;
            return;
        }

        Canvas.UpdateHealthValue(Health);
    }

    // increase time
    public void AddTime(float time) {
        TimeLeft += time;
    }

    public void DecreaseLeftTime(float time) {
        TimeLeft -= time;
        float percentage = TimeLeft / TotalTime;
        // necessary because of progress bar delay
        percentage = (percentage <= 0) ? TimeLeft=0 : percentage;
        Canvas.UpdateProgressBar(percentage);
    }
    
	void Update () {

        if (Input.GetKeyDown("space"))
        {
           if (isPaused)
            {
                isPaused = false;
                Audio.Play();
                Time.timeScale = 1;
            }
            else {
                Audio.Stop();
                Time.timeScale = 0;
                isPaused = true;
            }

            Canvas.TogglePauseButton(isPaused);


        }
            
        if (TimeLeft >= TotalTime || GameOver)
        {
            GameOver = true;
            StopGame();
            Canvas.ShowEndGame(GameOver);
            return;
        }
   
        // it is fixed, bad solution
        if (GameObject.FindGameObjectsWithTag("BoughtItem").Length == (int)GetCurrentLevelData()["amountOfItems"]) {
            StopGame();
            Canvas.ShowEndGame(GameOver);
            return;
        }
        AddTime(Time.deltaTime);
        Canvas.UpdateProgressBar(TimeLeft / TotalTime);
        

	}

    private void CallSpawningBuffs() {
        TimerScript.Spawn();
        CoinScript.Spawn();
        if (currentLevel >1) HeartScript.Spawn();
    }

    private void CallSpawningCobras()
    {
        if (SpawnedCobras >= SpawnedCobrasLimit) {
            CancelInvoke();
            return;
        }
        CobraController.SpawnCobra();
        SpawnedCobras++;
    }

    private void CallDisappearingBuffs() {
        TimerScript.Disappear();
        CoinScript.Disappear();
        if (currentLevel > 1) HeartScript.Disappear();
    }

    private void AddBudget(int amount) {
        Budget += amount;
    }

    public void DecreaseBudget(int amount)
    {
        Budget -= amount;
        Canvas.UpdateBudgetValue(Budget);
    }

    public void IncreaseBudget(int amount) {
        AddBudget(amount);
        Canvas.UpdateBudgetValue(Budget);
    }

    public int GetBudget()
    {
        return Budget;
    }

    public void AddItem(string itemname)
    {
        BoughtItems.Add(itemname);
    }

    public void StopGame() {
         Audio.Stop();
         Time.timeScale = 0;
    }

    public void PauseGame() {
        if (isPaused)
        {
            Audio.Play();
            Time.timeScale = 1;
            isPaused = false;
        } else {
            Audio.Stop();
            Time.timeScale = 0;
            isPaused = true;
        }
        Canvas.TogglePauseButton(isPaused);
        EventSystem.current.SetSelectedGameObject(null);

    }

    private void SpawnCustomers()
    {
        System.Random rndGenerator = new System.Random();
        Dictionary<string, object> data = LevelDataScript.GetLevelData(currentLevel);

        float min_x = (float) data["char_x_min"];
        float max_x = (float) data["char_x_max"];
        int stance_count = (int) data["nr_of_stances"];
        int customer_count = (int) data["cust_count"];
        float min_y = (float) data["y_min"];
        float max_y = (float) data["level_y_max"] - 2;

        float dist = max_x - min_x - 10;
        float cust_diff = dist / (customer_count - 1);

        float x = min_x + 5;
        for (int i = 0; i<customer_count; i++)
        {
            float y = rndGenerator.Next((int) min_y * 100, (int) max_y * 100) / 100;
            CustomerController customer = Instantiate(CustomerPrefab);
            customer.transform.position = new Vector3(x, y, 0);           
            customer.Level_data(data);
            customer.SetRandomGenerator(rndGenerator);
            x += cust_diff;
        }
    }


    public Dictionary<string, object> GetCurrentLevelData()
    {
        return LevelDataScript.GetLevelData(currentLevel);
    }

    private GameObject[] GetStands() {
        return GameObject.FindGameObjectsWithTag("Stand");
    }

    // this method creates randomly stands where player can negotiate about the price
    private void GenerateNegotiableStands() {
        int negotiableStands;
        GameObject[] stands = GetStands();
        // at least one seller of the stand wants to negotiate
        if (stands == null)
            negotiableStands = 5;
        else 
            negotiableStands = Random.Range(9, stands.Length);
        HashSet<int> negotiableStandIds = new HashSet<int>();
        //Debug.Log("count of negotiable stands " + negotiableStands);
  
        for (int i = 1; i <= negotiableStands; i++) {
            // get random stands which will be negotiable
            int randomId = Random.Range(0, stands.Length-1);
            while (negotiableStandIds.Contains(randomId)) {
                randomId = Random.Range(0, stands.Length - 1);
            }

            negotiableStandIds.Add(randomId);
        }


        HashSet<int>.Enumerator enumerator = negotiableStandIds.GetEnumerator();

        while (enumerator.MoveNext()) {
            Stand currentStand = stands[enumerator.Current].GetComponent<Stand>();
            // calculate min stand price
            currentStand.SetNegotiablePrice(true);
            currentStand.SetMinStandPrice(GenerateMinStandPrice(currentStand.GetMaxStandPrice()));
            
        }

        enumerator.Dispose();

    }

    private int GenerateMinStandPrice(int standMaxPrice) {
        // lower limit for min price
        int lowerLimit = standMaxPrice - Mathf.RoundToInt(standMaxPrice * PercentageDiscounts.GetPercentageDiscount());
        // random choose min price
        return Random.Range(lowerLimit, standMaxPrice);
    }

}
    