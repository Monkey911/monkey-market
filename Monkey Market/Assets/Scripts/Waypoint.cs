﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{

    public Waypoint Next;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		

	}

    void OnDrawGizmos()
    {
        if (Next == null)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, Next.transform.position);
    }

}
