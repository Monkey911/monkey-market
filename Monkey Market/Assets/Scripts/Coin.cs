﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Coin : MonoBehaviour {
    private Game GameInstance;
    public Coin CoinPrefab;

    private AudioSource AudioSource;
    private Coin NewCoin;
    private Queue<Coin> coins = new Queue<Coin>();
    private bool BuffAlreadyGained = false;         // with that I don't let player pick up same item multiple times

    private AudioClip AudioClip;

    // Use this for initialization
    void Start () {
        AudioSource = GetComponent<AudioSource>();
        GameInstance = Game.Instance;

        AudioClip = Resources.Load<AudioClip>("Sounds/coinSound");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Spawn()
    {
        if (!GameInstance.GameOver)
        {
            NewCoin = Instantiate(CoinPrefab);
            Vector3 newPosition = new Vector3(Random.Range(Game.MarketMinimumXValue, Game.MarketMaximumXValue),
                Random.Range(Game.MarketMinimumYValue, Game.MarketMaximumYValue), 0);

            while (!PositionManager.CheckIfPosEmpty(newPosition))
            {
                newPosition = new Vector3(Random.Range(Game.MarketMinimumXValue, Game.MarketMaximumXValue),
                    Random.Range(Game.MarketMinimumYValue, Game.MarketMaximumYValue), 0);
            }
            NewCoin.transform.position = newPosition;
            coins.Enqueue(NewCoin);
        }
    }

    void OnMouseDown()
    {
        if (PositionManager.CheckIfPlayerNear(gameObject.transform.position) && !BuffAlreadyGained)
            CoinPickedUp();
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player" && !BuffAlreadyGained)
            CoinPickedUp();
    }

    private void CoinPickedUp()
    {
        GameInstance.IncreaseBudget(10);
        BuffAlreadyGained = true;
        coins = new Queue<Coin>(coins.Where(s => s != gameObject));
        AudioSource.PlayOneShot(AudioClip);
        Destroy(gameObject, AudioClip.length);
        gameObject.GetComponent<Renderer>().enabled = false;
    }

    public void Disappear()
    {
        Coin firstCoin = coins.Dequeue();
        if (firstCoin != null && !GameInstance.GameOver && coins.Count != 0)
        {
            Destroy(firstCoin.gameObject);
        }
    }
}
