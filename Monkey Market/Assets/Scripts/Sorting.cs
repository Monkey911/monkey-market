﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sorting : MonoBehaviour {

    public float YValueFromCentre;

    private SpriteRenderer Sprite;

	void Start () {
        Sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update () {
        Sprite.sortingOrder = -(int)((transform.position.y + YValueFromCentre) * 100);
    }
}
