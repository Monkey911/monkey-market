﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class CustomerController : MonoBehaviour {

    //private SpriteRenderer sr;
    //private Animator animator;
    //private float latestDirectionChangeTime;
    //private readonly float directionChangeTime = 3f;
    //private float characterVelocity = 2f;
    //private Vector3 movementDirection;
    //private Vector3 movementPerSecond;

    private Animator CharacterAnimation;
    private SpriteRenderer CharacterRenderer;
    private Dictionary<string, object> data;
    private Rigidbody2D Prefab;
    private static System.Random RandomGenerator;

    //Here I instantiate customer action variables
    private float x_end;                // x position where the character wants to go
    private float y_end;                 // y position where the character wants to go
    private float temp_x_end;
    private float temp_y_end;
    private float subTemp_x_end;
    private float subTemp_y_end;
    private float subTemp2_x_end;
    private float subTemp2_y_end;
    private float x_to_change;
    private float y_to_change;
    private string state;
    private float last_check;
    private float last_x;
    private float last_y;

    //Queue variables
    private float buying_end;
    private float smallest_y_for_queue = -4;

    public float Speed;
    private float boxColliderWidth = 1.8f;
    private float boxColliderHeight = 0.9f;
    private List<CustomerController> nearbyObjects;

    void Awake()
    {
        CharacterAnimation = GetComponent<Animator>();
        CharacterRenderer = GetComponent<SpriteRenderer>();
        Prefab = GetComponent<Rigidbody2D>();
        nearbyObjects = new List<CustomerController>();
        state = "ok";
        AssignTemporaryEndpoint(-5000, -5000);
        AssignSubTemporaryEndpoint(-5000, -5000);
        AssignSubTemporary2Endpoint(-5000, -5000);
        this.last_check = Time.time;
        this.last_x = this.transform.position.x;
        this.last_y = this.transform.position.y;
    }

    void Update()
    {
        if (state == "ok")                  //cheching again if I will have problems
        {
            foreach (CustomerController customer in nearbyObjects)
            {
                if (WillCollide(customer))
                {
                    state = "will collide soon";
                    break;
                }
            }
        }
        else if (state == "will collide soon")
        {
            ChangeDirection();
            if (state != "forming queue")
                state = "ok";
        }
        else if (state == "leaving")
        {
            if (!SubTemp2EndDeclared())
                this.state = "ok";
        }
        else if (state == "buying")
        {
            EndpointReached();
            return;
        }
        else if (state == "forming queue")
        {
            if (!TempEndDeclared())                               //TODO MULTIPLE CUSTOMERS JOIN QUEUE AT THE SAME TIME
            {                                                        // TODO LEAVING THE LINE
                CharacterAnimation.SetInteger("Direction", 4);
                this.state = "ok";
                return;
            }
            FormLine();
        }
        else if (state == "queue")
        {
            MoveInQueue();
            if (!TempEndDeclared() && DistanceY(this, y_end) < this.boxColliderHeight)
            {
                CharacterAnimation.SetInteger("Direction", 4);
                return;
            }
        }
        TransformCustomer();
        if (AnythingWrong())
        {
            AssignTemporaryEndpoint(-5000, -5000);
            AssignSubTemporaryEndpoint(-5000, -5000);
            AssignSubTemporary2Endpoint(-5000, -5000);
            DeclareEndPoint();
        }
    }


    private void SetAnimation(float x, float y)
    {
        if (x < 0.02 && x > -0.02 && y < 0.04 && y > -0.04)          // Character is idle 
            CharacterAnimation.SetInteger("Direction", 0);
        else if (y >= 0.04)
        {
            CharacterAnimation.SetInteger("Direction", 3);         // Moving up
        }
        else if (y < -0.04)
        {
            CharacterAnimation.SetInteger("Direction", 2);                  // Moving down
            if (x > 0)
                CharacterRenderer.flipX = false;
            else
                CharacterRenderer.flipX = true;
        }
       else if (Math.Abs(x) >= Math.Abs(y))
        {
            CharacterAnimation.SetInteger("Direction", 1);
            if (x > 0)                                                  // Moving right
                CharacterRenderer.flipX = false;
            else                                                        // Moving left
                CharacterRenderer.flipX = true;
        }
        else if (Math.Abs(y) > Math.Abs(x))
        {
            if (y > 0)
                CharacterAnimation.SetInteger("Direction", 3);         // Moving up
            else
                CharacterAnimation.SetInteger("Direction", 2);         // Moving down
        }
        else
        {
            throw new InvalidOperationException("Uncovered x = " + x + " and y = " + y + " change in setting animation");
        }
    }

    private void TransformCustomer()
    {
        if (SubTemp2EndDeclared())
        {
            float x_dist_from_temp = subTemp2_x_end - transform.position.x;
            float y_dist_from_temp = subTemp2_y_end - transform.position.y;
            if (!((x_dist_from_temp > 0.1 || x_dist_from_temp < -0.1) || (y_dist_from_temp > 0.1 || y_dist_from_temp < -0.1)))     // I'm close to sub temporary point
            {
                AssignSubTemporary2Endpoint(-5000, -5000);
                if (SubTempEndDeclared())
                    SetMovementsTowardsSubTemp();
                else if (TempEndDeclared())
                    SetMovementsTowardsTemp();
                else
                    SetMovementsTowardsEnd();
            }
        }
        if (SubTempEndDeclared())
        {
            float x_dist_from_temp = subTemp_x_end - transform.position.x;
            float y_dist_from_temp = subTemp_y_end - transform.position.y;
            if (!((x_dist_from_temp > 0.1 || x_dist_from_temp < -0.1) || (y_dist_from_temp > 0.1 || y_dist_from_temp < -0.1)))     // I'm close to sub temporary point
            {
                AssignSubTemporaryEndpoint(-5000, -5000);
                if (TempEndDeclared())
                    SetMovementsTowardsTemp();
                else
                    SetMovementsTowardsEnd();
            }
        }
        else if (TempEndDeclared())
        {
            float x_dist_from_temp = temp_x_end - transform.position.x;
            float y_dist_from_temp = temp_y_end - transform.position.y;
            if (!((x_dist_from_temp > 0.4 || x_dist_from_temp < -0.4) || (y_dist_from_temp > 0.4 || y_dist_from_temp < -0.4)))     // I'm close to temporary point
            {
                AssignTemporaryEndpoint(-5000, -5000);
                SetMovementsTowardsEnd();
            }
        }
        else
        {
            float x_change_from_endpoint = x_end - transform.position.x;
            float y_change_from_endpoint = y_end - transform.position.y;
            if ((x_change_from_endpoint < 0.05 && x_change_from_endpoint > -0.05) && (y_change_from_endpoint < 0.05 && y_change_from_endpoint > -0.05))      // I'm close to the endpoint
            {
                EndpointReached();
                return;
            }
        }
        ChangePosition();
    }

    private void ChangePosition()
    {
        float x = Time.deltaTime * x_to_change;
        float y = Time.deltaTime * y_to_change;
        SetAnimation(x, y);
        Prefab.MovePosition(new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z));
    }

    public void Level_data(Dictionary<string, object> data)
    {
        this.data = data;
    }

    private void DeclareEndPoint()
    {
        int destination = RandomGenerator.Next(1, 18);
        //int destination = RandomGenerator.Next(1, 14);
        //destination = 3;
        if (destination <= (int) data["nr_of_stances"])
        {
            List<float> stancePos = data[destination.ToString()] as List<float>;
            AssignEndpoint(stancePos[0], stancePos[1]);
        }
        else                                            // we are going out from the market
        {
            int hPercentage = RandomGenerator.Next(1, 100);
            float market_area_height = (float)data["y_max"] - (float)data["y_min"];
            if (destination == 14 || destination == 15)    // Go out from the left
                AssignEndpoint((float)data["x_min"], (float)data["y_min"] + market_area_height * hPercentage / 100);
            else                                            //go out from the right
                AssignEndpoint((float)data["x_max"], (float)data["y_min"] + market_area_height * hPercentage / 100);
        }
    }

    private void DeclareSubTemporayEndpoint()
    {
        if (x_end > transform.position.x)
        {
            AssignSubTemporaryEndpoint(transform.position.x + 5, -4);
            SetMovementsTowardsSubTemp();
        }
        else if (x_end < transform.position.x)
        {
            AssignSubTemporaryEndpoint(transform.position.x - 5, -4);
            SetMovementsTowardsSubTemp();
        }
    }

    private void DeclareSubTemporay2Endpoint()
    {
        if (x_end > transform.position.x)
        {
            AssignSubTemporary2Endpoint(transform.position.x + 2, transform.position.y - 0.3f);
        }
        else if (x_end < transform.position.x)
        {
            AssignSubTemporary2Endpoint(transform.position.x - 2, transform.position.y - 0.3f);
        }
        SetMovementsTowardsSubTemp2();
    }


    public void SetRandomGenerator(System.Random generator) {
        RandomGenerator = generator;
        DeclareEndPoint();
    }

    private void EndpointReached()
    {
        if (state == "buying")
        {
            CharacterAnimation.SetInteger("Direction", 4);
            DeclareEndPoint();

            if (Time.time > buying_end)
            {
                if (x_end - transform.position.x < 2 && x_end - transform.position.y > -2)          // I buy something more
                {
                    buying_end = Time.time + 2;
                }
                else
                {
                    DeclareSubTemporayEndpoint();
                    DeclareSubTemporay2Endpoint();
                    state = "leaving";
                }
            }
        }
        else
        {
            CharacterAnimation.SetInteger("Direction", 4);
            buying_end = Time.time + 2;
            state = "buying";
        }
    }

    private void SetMovementsTowardsEnd()
    {
        SetMovementValues(x_end - transform.position.x, y_end - transform.position.y);
    }

    private void SetMovementsTowardsTemp()
    {
        SetMovementValues(temp_x_end - transform.position.x, temp_y_end - transform.position.y);
    }

    private void SetMovementsTowardsSubTemp()
    {
        SetMovementValues(subTemp_x_end - transform.position.x, subTemp_y_end - transform.position.y);
    }

    private void SetMovementsTowardsSubTemp2()
    {
        SetMovementValues(subTemp2_x_end - transform.position.x, subTemp2_y_end - transform.position.y);
    }

    private void SetMovementValues(float x, float y)
    {
        float x_change, y_change;
        float x_val = Math.Abs(x);
        float y_val = Math.Abs(y);
        if (x_val >= y_val)
        {
            x_change = this.Speed;
            y_change = this.Speed * y_val / x_val;
        }
        else
        {
            y_change = this.Speed;
            x_change = this.Speed * x_val / y_val;
        }

        if (x >= 0)
            this.x_to_change = x_change;
        else
            this.x_to_change = -x_change;
        if (y >= 0)
            this.y_to_change = y_change;
        else
        {
            this.y_to_change = -y_change;
        }
    }

    private void AssignTemporaryEndpoint(float x, float y)
    { 
        this.temp_x_end = x;
        this.temp_y_end = y;
        SetMovementsTowardsTemp();
    }


    private void AssignEndpoint(float x, float y)
    {
        this.x_end = x;
        this.y_end = y;
        if (x - transform.position.x > 5)
            AssignTemporaryEndpoint(x_end - 5, y_end - 4);
        else if (x - transform.position.x < -5)
            AssignTemporaryEndpoint(x_end + 5, y_end - 4);
        else
        {
            SetMovementsTowardsEnd();
        }
    }

    private void AssignSubTemporaryEndpoint(float x, float y)
    {
        this.subTemp_x_end = x;
        this.subTemp_y_end = y;
        SetMovementsTowardsSubTemp();
    }

    private void AssignSubTemporary2Endpoint(float x, float y)
    {
        this.subTemp2_x_end = x;
        this.subTemp2_y_end = y;
        SetMovementsTowardsSubTemp2();
    }

    public Boolean TempEndDeclared()
    {
        if (temp_x_end == -5000 && temp_y_end == -5000)
            return false;
        return true;
    }

    public Boolean SubTempEndDeclared()
    {
        if (subTemp_x_end == -5000 && subTemp_y_end == -5000)
            return false;
        return true;
    }

    public Boolean SubTemp2EndDeclared()
    {
        if (subTemp2_x_end == -5000 && subTemp2_y_end == -5000)
            return false;
        return true;
    }

    private Boolean WillCollide(CustomerController c)
    {
        float dx = Math.Abs(c.transform.position.x - this.transform.position.x);
        float t;

        if (c.transform.position.x >= this.transform.position.x && c.x_to_change < this.x_to_change)
            t = dx / (this.x_to_change - c.x_to_change);
        else if (c.transform.position.x < this.transform.position.x && c.x_to_change > this.x_to_change)
            t = dx / (c.x_to_change - this.x_to_change);
        else if (dx < boxColliderWidth)
        {
            if (c.transform.position.y >= this.transform.position.y)
                t = dx / (this.y_to_change - c.y_to_change);
            else
                t = dx / (c.y_to_change - this.y_to_change);
        }
        else
            return false;

        float my_future_x = t * this.x_to_change + this.transform.position.x;
        float c_future_x = t * c.x_to_change + c.transform.position.x;
        float my_future_y = t * this.y_to_change + this.transform.position.y;
        float c_future_y = t * c.y_to_change + c.transform.position.y;

        if (Math.Abs(my_future_x - c_future_x) > boxColliderWidth || Math.Abs(my_future_y - c_future_y) > boxColliderHeight)
            return false;
        return true;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Customer")
        {
            CustomerController customer = collider.GetComponent<CustomerController>();
            nearbyObjects.Add(customer);
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Customer")
        {
            nearbyObjects.Remove(collider.GetComponent<CustomerController>());
        }
    }


    private void ChangeDirection()
    {
        foreach (CustomerController customer in nearbyObjects)
        {
            if (WillCollide(customer))
            {
                if ((customer.state == "buying" || customer.state == "queue") && DistanceX(customer, x_end) < 0.05)
                {
                    FormLine();
                    return;
                }
                float x_change;
                if (x_to_change > 0)
                {
                    x_change = (DistanceX(this, customer) / 2 - boxColliderWidth / 2) * 0.95f;
                }
                else
                {
                    x_change = (DistanceX(this, customer) / 2 - boxColliderWidth / 2) * -0.95f;
                }
                float y_change;
                if (this.transform.position.y > customer.transform.position.y)
                    y_change = boxColliderWidth / 2 - DistanceY(this, customer) + 0.25f;
                else
                    y_change = DistanceY(this, customer) - boxColliderWidth / 2 - 0.25f;
                
                AssignSubTemporaryEndpoint(this.transform.position.x + x_change, this.transform.position.y + y_change);
            }
        }
    }

    private double Distance(CustomerController customer1, CustomerController customer2)
    {
        double x = Math.Abs(customer1.transform.position.x - customer2.transform.position.x);
        double y = Math.Abs(customer1.transform.position.y - customer2.transform.position.y);
        return (Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2)));
    }

    private float DistanceX(CustomerController customer1, CustomerController customer2)
    {
        return (Math.Abs(customer1.transform.position.x - customer2.transform.position.x));
    }

    private double DistanceX(CustomerController customer1, double cust_x)
    {
        return (Math.Abs(customer1.transform.position.x - cust_x));
    }


    private float DistanceY(CustomerController customer1, CustomerController customer2)
    {
        return (Math.Abs(customer1.transform.position.y - customer2.transform.position.y));
    }

    private double DistanceY(CustomerController customer1, double cust_y)
    {
        return (Math.Abs(customer1.transform.position.y - cust_y));
    }

    private float Max(float x, float y)
    {
        if (x > y)
            return x;
        return y;
    }

    private float Min(float x, float y)
    {
        if (x < y)
            return x;
        return y;
    }

    private void FormLine()
    {
        float y_min = y_end;                                        // finding the last customer in the market
        foreach (CustomerController customer in nearbyObjects)
        {
            if ((customer.state == "buying" || customer.state == "queue") && DistanceX(customer, x_end) < 0.05 && customer.transform.position.y < y_min)
            {
                y_min = customer.transform.position.y;
            }
        }
        float predicted_y = y_min - boxColliderHeight;
        if (predicted_y > this.smallest_y_for_queue)
            FormQueue(predicted_y);
        else
        {
            DeclareEndPoint();
            DeclareSubTemporayEndpoint();
        }
    }

    private void FormQueue(float y)
    {
        this.state = "forming queue";
        AssignTemporaryEndpoint(x_end, y);
    }

    private void MoveInQueue()
    {
        float y_min = y_end;                                        // finding the last customer in front of me
        foreach (CustomerController customer in nearbyObjects)
        {
            if (Math.Abs(customer.transform.position.x - this.transform.position.x) < boxColliderWidth &&
                customer.transform.position.y > this.transform.position.y)
            {
                if (y_min > customer.transform.position.y)
                    y_min = customer.transform.position.y;
            }            
        }
       if (y_min == y_end)
        {
            this.state = "ok";
            SetMovementsTowardsEnd();
        }
       else
        {
            this.state = "forming queue";
            FormLine();
        }
    }

    private void ResetDirection()
    {
        if (SubTempEndDeclared())
            SetMovementsTowardsSubTemp();
        else if (TempEndDeclared())
            SetMovementsTowardsTemp();
        else if (TempEndDeclared())
            SetMovementsTowardsEnd();
    }

    private Boolean AnythingWrong()
    {
       /*if (x_to_change > 0 && this.transform.position.x > x_end)
            return true;
        if (x_to_change < 0 && this.transform.position.x < x_end)
            return true;*/
        if (Time.time > last_check + 1)
        {
            /*if (this.transform.position.x >= last_x && x_end < last_x)
                return true;
            if (this.transform.position.x <= last_x && x_end > last_x)
                return true;*/
            if (Math.Abs(this.transform.position.x - this.last_x) < 0.05 && (this.state != "buying" || this.state!="queue" || this.state != "forming queue"))
            {
                last_check = Time.time;
                last_x = this.transform.position.x;
                last_y = this.transform.position.y;
                return true;
            }
            if (Math.Abs(this.transform.position.x - this.last_x) < 0.2 && Math.Abs(this.transform.position.y - this.last_y) < 0.1 && 
                (this.state != "buying" || this.state != "queue" || this.state != "forming queue"))
            {
                last_check = Time.time;
                last_x = this.transform.position.x;
                last_y = this.transform.position.y;
                return true;
            }
            last_check = Time.time;
            last_x = this.transform.position.x;
            last_y = this.transform.position.y;
        }
        if (this.transform.position.y < (float) data["y_min"]) {
            return true;
        }
        return false;
    }
}





