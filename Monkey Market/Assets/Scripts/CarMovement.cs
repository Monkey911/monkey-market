﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour {
    public int Damage;
    public float maxTime;
    public float minTime;
    
    private AudioClip AudioClip;
    private AudioSource AudioSource;
    private int TypeOfCars;

    void Start()
    {
        float random = Random.Range(minTime, maxTime);
        Invoke("SpawnCar", random);
        AudioSource = GetComponent<AudioSource>();
        AudioClip = Resources.Load<AudioClip>("Sounds/CarCrash");
        TypeOfCars = (int)Game.Instance.GetCurrentLevelData()["typeOfCars"];

    }


    // Update is called once per frame
    void Update () {

        if (gameObject.transform.position.x > (float)Game.Instance.GetCurrentLevelData()["x_max"])
        {
            Destroy(gameObject);
           
        }

	}

    void SpawnCar()
    {
        CancelInvoke();

       int randomCar = Random.Range(1, TypeOfCars);
        Object objTemp = null;

 
        switch (randomCar)
        {
            case 1:
                objTemp = Resources.Load("Prefabs/Car");
                break;
            case 2:
                objTemp =  Resources.Load("Prefabs/Rickshaw");
                break;
            case 3:
                objTemp = Resources.Load("Prefabs/Truck");
                break;
            case 4:
                objTemp = Resources.Load("Prefabs/Ricksaw2");
                break;
            default:
                objTemp = Resources.Load("Prefabs/Car");
                break;
        }
        if (objTemp != null) Instantiate(objTemp);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && Game.Instance.currentLevel > 1)
        {
            Game.Instance.DecreaseHealth(Damage);
            Game.Instance.Canvas.StartCoroutine(Game.Instance.Canvas.ShowMessage("CarHit"));
            AudioSource.PlayOneShot(AudioClip);
        }
        if (collision.tag == "Player")
        {
            collision.transform.position = new Vector3(collision.transform.position.x, this.transform.position.y + 3, collision.transform.position.z);
        }

    }
}
