﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoppingItem  {

    private int price;
    private string name;
    private string description;

    public ShoppingItem(string name, string description, int price)
    {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    public int Price
    {
        get { return price; }
        set { price = value; }
    }

    public string Description
    {
        get { return description; }
        set { description = value; }
    }


}
