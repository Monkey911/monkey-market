﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInformation : MonoBehaviour {

    public Camera Camera;

    static List<ShoppingItem> baseShoppingItems = new List<ShoppingItem>
    {
        new ShoppingItem("Fruit", "1x Apple", 13),
        new ShoppingItem("Fruit", "1x Orange", 13),
        new ShoppingItem("Fish", "1x Fish", 37),
        new ShoppingItem("Melon", "1x Melon", 32),
        new ShoppingItem("Necklace", "1x Necklace", 45),
        new ShoppingItem("Vase", "1x Vase", 75),
        new ShoppingItem("Pineapple", "1x Pineapple", 22),
        new ShoppingItem("Banana", "1x Banana", 15),

    };

    static List<ShoppingItem> shoppingItemsLevel2 = baseShoppingItems.ConvertAll(x => x);
    static List<ShoppingItem> shoppingItemsLevel3 = shoppingItemsLevel2.ConvertAll(x => x);


    private Dictionary<int, Dictionary<string, object>> dict;
    //Dict[level] -> dict[what's in it] ->
    // I probably need string values like: [x_max, x_min, y_max, y_min, stances]

    // Use this for initialization
    void Awake () {
        dict = new Dictionary<int, Dictionary<string, object>>();
        dict.Add(1, LevelOneData());
        dict.Add(2, LevelTwoData());
        dict.Add(3, LevelThreeData());
    }

    // Update is called once per frame
    void Update () {
		
	}

    public static List<ShoppingItem> GetShoppingItemForLevel (int currentLevel)
    {
        if (currentLevel > 1) {
            shoppingItemsLevel2.Add(new ShoppingItem("Onion", "1x Onion", 11));
            shoppingItemsLevel2.Add(new ShoppingItem("Pepper", "1x Pepper", 16));

            if (currentLevel == 3)
            {
                shoppingItemsLevel3.Add(new ShoppingItem("Curry", "1x Curry sauce", 32));
                shoppingItemsLevel3.Add(new ShoppingItem("Garlic", "1x Garlic", 19));
                shoppingItemsLevel3.Add(new ShoppingItem("Tomato", "1x Tomato", 22));

            }

            return currentLevel == 2 ? shoppingItemsLevel2 : shoppingItemsLevel3;

        }

 

        return baseShoppingItems;
    }

    public Dictionary<string, object> GetLevelData(int level)
    {
        return dict[level];
    }

    private Dictionary<string, object> LevelOneData()
    {
        Dictionary<string, object> dict1 = new Dictionary<string, object>();
        dict1.Add("x_max", 120f);
        dict1.Add("x_min", -30f);
        dict1.Add("y_max", 3.4584f);
        dict1.Add("y_min", -6.9f);

        dict1.Add("level_y_max", -0.22f);

        dict1.Add("char_x_max", 114.7f);
        dict1.Add("char_x_min", -24.0f);

        //Started adding stances
        dict1.Add("nr_of_stances", 13);
        dict1.Add("1", new List<float> { -21.17521f, -0.2448368f });
        dict1.Add("2", new List<float> { -13.21929f, -0.267952f });
        dict1.Add("3", new List<float> { -5.469269f, -0.2679515f });
        dict1.Add("4", new List<float> { 17.7161f, -0.2161498f });
        dict1.Add("5", new List<float> { 26.22131f, -0.2679515f });
        dict1.Add("6", new List<float> { 34.87757f, -0.2448368f });
        dict1.Add("7", new List<float> { 43.17731f, -0.2811966f });
        dict1.Add("8", new List<float> { 51.27583f, -0.267952f });
        dict1.Add("9", new List<float> { 66.85458f, -0.254426f });
        dict1.Add("10", new List<float> { 74.81956f, -0.3052573f });
        dict1.Add("11", new List<float> { 82.97831f, -0.2710564f });
        dict1.Add("12", new List<float> { 91.20421f, -0.2811966f });
        dict1.Add("13", new List<float> { 99.29025f, -0.2161498f });

        dict1.Add("cust_count", 13);

        dict1.Add("amountOfItems", 5);

        dict1.Add("lessBudgetAbout", 0);

        dict1.Add("amountOfCobras", 5);

        dict1.Add("health", 0);
        dict1.Add("typeOfCars", 1);


        float cameraMinXPosition;
        float cameraMaxXPosition;
        double aspect_ratio = System.Math.Round(Camera.aspect, 2);
        if (aspect_ratio == 1.78)
        {
            cameraMinXPosition = 5;
            cameraMaxXPosition = 85.7f;
        }
        else if (aspect_ratio == 1.6)
        {
            cameraMinXPosition = 2;
            cameraMaxXPosition = 88.8f;
        }
        else if (aspect_ratio == 1.5)
        {
            cameraMinXPosition = 0.2f;
            cameraMaxXPosition = 90.5f;
        }
        else if (aspect_ratio == 1.33)
        {
            cameraMinXPosition = -2.7f;
            cameraMaxXPosition = 93.4f;
        }
        else if (aspect_ratio == 1.25)
        {
            cameraMinXPosition = -4.1f;
            cameraMaxXPosition = 94.85f;
        }
        else
        {
            cameraMinXPosition = 5;
            cameraMaxXPosition = 85.7f;
        }

        dict1.Add("camera_x_max", cameraMaxXPosition);
        dict1.Add("camera_x_min", cameraMinXPosition);

        return dict1;
    }


    private Dictionary<string, object> LevelTwoData()
    {
        Dictionary<string, object> dict1 = new Dictionary<string, object>();
        dict1.Add("x_max", 192.6f);
        dict1.Add("x_min", -23.6f);
        dict1.Add("y_max", 0.4f);
        dict1.Add("y_min", -9.1f);

        dict1.Add("level_y_max", -0.022f);
        dict1.Add("char_x_max", 192.6f);
        dict1.Add("char_x_min", -23.8f);

        dict1.Add("nr_of_stances", 21);
        dict1.Add("cust_count", 17);
        //Started adding stances

        float y_coord = -0.003885031f;
        dict1.Add("1", new List<float> { -21.40231f, -y_coord });
        dict1.Add("2", new List<float> { -13.01406f, y_coord });
        dict1.Add("3", new List<float> { -5.198925f, y_coord });
        dict1.Add("4", new List<float> { 26.205f, y_coord });
        dict1.Add("5", new List<float> { 34.94137f, y_coord });
        dict1.Add("6", new List<float> { 43.20734f, y_coord });
        dict1.Add("7", new List<float> { 51.51572f, y_coord });
        dict1.Add("8", new List<float> { 66.74704f, y_coord });
        dict1.Add("9", new List<float> { 75.03831f, y_coord });
        dict1.Add("10", new List<float> { 83.16994f, y_coord });
        dict1.Add("11", new List<float> { 91.3013f, y_coord });
        dict1.Add("12", new List<float> { 99.17705f, y_coord });
        dict1.Add("13", new List<float> { 107.7519f, y_coord });
        dict1.Add("14", new List<float> { 116.7877f, y_coord });
        dict1.Add("15", new List<float> { 125.0833f, y_coord });
        dict1.Add("16", new List<float> { 147.6523f, y_coord });
        dict1.Add("17", new List<float> { 155.5991f, y_coord });
        dict1.Add("18", new List<float> { 163.7388f, y_coord });
        dict1.Add("19", new List<float> { 172.1158f, y_coord });
        dict1.Add("20", new List<float> { 180.8309f, y_coord });
        dict1.Add("21", new List<float> { 189.4461f, y_coord });

        dict1.Add("amountOfItems", 7);

        dict1.Add("lessBudgetAbout", Random.Range(15, 20));

        dict1.Add("amountOfCobras", Random.Range(5, 10));

        dict1.Add("health", 100);
        dict1.Add("typeOfCars", 2);

        float cameraMinXPosition;
        float cameraMaxXPosition;
        double aspect_ratio = System.Math.Round(Camera.aspect, 2);

        if (aspect_ratio == 1.78)
        {
            cameraMinXPosition = 7.75f;
            cameraMaxXPosition = 160.95f;
        }
        else if (aspect_ratio == 1.6)
        {
            cameraMinXPosition = 4.4f;
            cameraMaxXPosition = 164.3f;
        }
        else if (aspect_ratio == 1.5)
        {
            cameraMinXPosition = 2.6f;
            cameraMaxXPosition = 166;
        }
        else if (aspect_ratio == 1.33)
        {
            cameraMinXPosition = -0.5f;
            cameraMaxXPosition = 169.2f;
        }
        else if (aspect_ratio == 1.25)
        {
            cameraMinXPosition =    -2f;
            cameraMaxXPosition = 170.7f;
        }
        else
        {
            cameraMinXPosition = 7.75f;
            cameraMaxXPosition = 160.95f;
        }

        dict1.Add("camera_x_max", cameraMaxXPosition);
        dict1.Add("camera_x_min", cameraMinXPosition);
        return dict1;
    }



    private Dictionary<string, object> LevelThreeData()
    {
        Dictionary<string, object> dict1 = new Dictionary<string, object>();
        dict1.Add("x_max", 281.5f);
        dict1.Add("x_min", -13.6f);
        dict1.Add("y_max", 0.4f);
        dict1.Add("y_min", -15.1f);

        dict1.Add("char_x_max", 282.6869f);
        dict1.Add("char_x_min", -23.8f);
        dict1.Add("level_y_max", 0.1341256f);

        dict1.Add("nr_of_stances", 21);
        dict1.Add("cust_count", 17);
        //Started adding stances

        float y_coord = 0.1341256f;
        dict1.Add("1", new List<float> { -21.2248f, -y_coord });
        dict1.Add("2", new List<float> { -13.02225f, y_coord });
        dict1.Add("3", new List<float> { -4.636096f, y_coord });
        dict1.Add("4", new List<float> { 18.18675f, y_coord });
        dict1.Add("5", new List<float> { 26.31413f, y_coord });
        dict1.Add("6", new List<float> { 34.91875f, y_coord });
        dict1.Add("7", new List<float> { 43.21939f, y_coord });
        dict1.Add("8", new List<float> { 51.34641f, y_coord });
        dict1.Add("9", new List<float> { 66.75732f, y_coord });
        dict1.Add("10", new List<float> { 74.9723f, y_coord });
        dict1.Add("11", new List<float> { 83.09211f, y_coord });
        dict1.Add("12", new List<float> { 91.57113f, y_coord });
        dict1.Add("13", new List<float> { 99.5051f, y_coord });
        dict1.Add("14", new List<float> { 107.8975f, y_coord });
        dict1.Add("15", new List<float> { 116.7765f, y_coord });
        dict1.Add("16", new List<float> { 125.2358f, y_coord });
        dict1.Add("17", new List<float> { 147.0389f, y_coord });
        dict1.Add("18", new List<float> { 155.6281f, y_coord });
        dict1.Add("19", new List<float> { 163.8409f, y_coord });
        dict1.Add("20", new List<float> { 172.2013f, y_coord });
        dict1.Add("21", new List<float> { 180.619f, y_coord });
        dict1.Add("22", new List<float> { 189.4686f, y_coord });
        dict1.Add("23", new List<float> { 197.4891f, y_coord });
        dict1.Add("24", new List<float> { 205.8923f, y_coord });
        dict1.Add("25", new List<float> { 214.3217f, y_coord });
        dict1.Add("26", new List<float> { 222.8127f, y_coord });
        dict1.Add("27", new List<float> { 231.0562f, y_coord });
        dict1.Add("28", new List<float> { 239.4908f, y_coord });
        dict1.Add("29", new List<float> { 247.6158f, y_coord });
        dict1.Add("30", new List<float> { 255.5322f, y_coord });
        dict1.Add("31", new List<float> { 263.7425f, y_coord });
        dict1.Add("32", new List<float> { 272.0962f, y_coord });
        dict1.Add("33", new List<float> { 280.0115f, y_coord });

        dict1.Add("amountOfItems", 10);

        dict1.Add("lessBudgetAbout", Random.Range(25, 40));

        dict1.Add("amountOfCobras", Random.Range(14, 20));

        dict1.Add("health", 70);
        dict1.Add("typeOfCars", 4);

        float cameraMinXPosition;
        float cameraMaxXPosition;
        double aspect_ratio = System.Math.Round(Camera.aspect, 2);

        if (aspect_ratio == 1.78)
        {
            cameraMinXPosition = 7.901342f;
            cameraMaxXPosition = 250.9425f;
        }
        else if (aspect_ratio == 1.6)
        {
            cameraMinXPosition = 4.4f;
            cameraMaxXPosition = 164.3f;
        }
        else if (aspect_ratio == 1.5)
        {
            cameraMinXPosition = 2.6f;
            cameraMaxXPosition = 166;
        }
        else if (aspect_ratio == 1.33)
        {
            cameraMinXPosition = -0.5f;
            cameraMaxXPosition = 169.2f;
        }
        else if (aspect_ratio == 1.25)
        {
            cameraMinXPosition = -2f;
            cameraMaxXPosition = 170.7f;
        }
        else
        {
            cameraMinXPosition = 7.75f;
            cameraMaxXPosition = 160.95f;
        }

        dict1.Add("camera_x_max", cameraMaxXPosition);
        dict1.Add("camera_x_min", cameraMinXPosition);
        return dict1;
    }


}
