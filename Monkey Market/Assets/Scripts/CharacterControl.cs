﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CharacterControl : MonoBehaviour
{

    public float Speed;

    private Animator CharacterAnimation;
    private SpriteRenderer CharacterRenderer;
    private float CharacterMinimumXValue;
    private float CharacterMaximumXValue;
    private Rigidbody2D Character;

    void Start()
    {
        CharacterAnimation = GetComponent<Animator>();
        CharacterRenderer = GetComponent<SpriteRenderer>();
        Character = GetComponent<Rigidbody2D>();
        int screenWidth = Screen.currentResolution.width;
        CharacterMaximumXValue = (float)Game.Instance.GetCurrentLevelData()["char_x_max"];
        CharacterMinimumXValue = (float)Game.Instance.GetCurrentLevelData()["char_x_min"];
    }

    void Update()
    {
        float y_movement = Input.GetAxis("Vertical");
        float x_movement = Input.GetAxis("Horizontal");

        SetAnimation(x_movement, y_movement);
        TransformCharacter(x_movement, y_movement);
        CharacterRenderer.sortingOrder = -(int)(transform.position.y * 100);            // sets sorting lavel based on y coordinate
    }

    private void SetAnimation(float x, float y)
    {
        if (x < 0.05 && x > -0.05 && y < 0.05 && y > -0.05)          // Character is idle 
            CharacterAnimation.SetInteger("Direction", 0);
        else if (Math.Abs(x) >= Math.Abs(y))
        {
            CharacterAnimation.SetInteger("Direction", 1);
            if (x > 0)                                                  // Moving right
                CharacterRenderer.flipX = false;
            else                                                        // Moving left
                CharacterRenderer.flipX = true;
        }
        else if (y > 0.05)
            CharacterAnimation.SetInteger("Direction", 3);                  // Moving up
        else if (y < -0.05)
        {
            CharacterAnimation.SetInteger("Direction", 2);                  // Moving down
            if (x > 0)
                CharacterRenderer.flipX = false;
            else
                CharacterRenderer.flipX = true;
        }
        else
            throw new InvalidOperationException("Uncovered x and y change in setting animation");
    }

    private void TransformCharacter(float x_change, float y_change)
    {
        x_change *= Time.deltaTime * Speed;
        y_change *= Time.deltaTime * Speed;

        float x_value = transform.position.x + x_change;
        if (x_value < CharacterMinimumXValue)
            x_value = CharacterMinimumXValue;
        else if (x_value > CharacterMaximumXValue)
            x_value = CharacterMaximumXValue;
        //transform.position = new Vector3(x_value, transform.position.y + y_change, transform.position.z);
        Character.MovePosition(new Vector3(x_value, transform.position.y + y_change, transform.position.z));
    }


}
