﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShoppingListScript : MonoBehaviour {

    private Game gameInstance;

    static List<ShoppingItem> shoppingItems;

    // Use this for initialization
    void Start () {
        gameInstance = Game.Instance;
        shoppingItems = LevelInformation.GetShoppingItemForLevel(gameInstance.currentLevel);
        GenerateShoppingList();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void GenerateShoppingList() {
        List<ShoppingItem> generatedShoppingList2 = new List<ShoppingItem>();
        int budget = 0;

        int amountOfItems = (int)gameInstance.GetCurrentLevelData()["amountOfItems"];

        for (int i = 1; i <= amountOfItems; i++)
        {
            ShoppingItem randomItem = shoppingItems[Random.Range(0, shoppingItems.Count)];
            while (generatedShoppingList2.Contains(randomItem))
            {
                randomItem = shoppingItems[Random.Range(0, shoppingItems.Count)];
            }
            generatedShoppingList2.Add(randomItem);
            budget += randomItem.Price;

        }

        budget -= (int)gameInstance.GetCurrentLevelData()["lessBudgetAbout"];

        gameInstance.Canvas.SetShoppingItems(generatedShoppingList2);
        gameInstance.IncreaseBudget(budget);
    }
}
