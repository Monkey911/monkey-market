﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager {


    public static bool CheckIfPlayerNear(Vector3 itemPosition)
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (Vector3.Distance(player.transform.position, itemPosition) <= 2.5)
        {
            return true;
        }
        return false;
    }
}
