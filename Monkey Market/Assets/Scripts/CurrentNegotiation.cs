﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentNegotiation : MonoBehaviour {
    // necessary to save from whom the price was
    private SellerScript seller;
    private int currentPrice;

    public void SetCurrentPrice(int price)
    {
        currentPrice = price;
    }

    public int GetCurrentPrice() {
        return currentPrice;
    }

    public void SetSeller(SellerScript seller) {
        this.seller = seller;
    }

    public SellerScript GetSeller() {
        return seller;
    }
}
