﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellerScript : MonoBehaviour {
    public Game GameInstance;
    public Canvas bubbleCanvas;
    public Text negotiableText;
    public Text negotiablePrice;
    public Stand stand;
    public int guessTolerance;
    private GameObject negotiationPanel;
   
    // variables for autotyping
    public float delay = 0.1f;
    public string fullText;
    private string tempString = "";
    private bool negotiation = false;


    void Start () {
        //Debug.Log(Mathf.Clamp(5, 10, 40));
        fullText = negotiableText.text;
        if (GameInstance == null) GameInstance = Game.Instance;
        // negotiation panel isnt active, I had to find parent
        if (negotiationPanel == null) {
            Transform[] trans = GameObject.Find("Canvas").GetComponentsInChildren<Transform>(true);
            foreach (Transform t in trans)
            {
                if (t.gameObject.name == "NegotiationPanel")
                {
                    negotiationPanel = t.gameObject;
                }
            }
        } 

    }
	
	// Update is called once per frame
	void Update () {
        if (negotiation == true && !PlayerManager.CheckIfPlayerNear(gameObject.transform.position)) {
            stand.SetCurrentPrice(stand.GetMaxStandPrice());
            negotiation = false;
        }

    }

    void OnMouseDown()
    {
        if (stand == null) return;
        if (!PlayerManager.CheckIfPlayerNear(gameObject.transform.position))
        {
            GameInstance.Canvas.StartCoroutine(GameInstance.Canvas.ShowMessage("Distance"));
            return;
        } else {
            if (stand.IsNegotiablePrice())
            {
                    if (negotiationPanel == null || negotiationPanel.activeSelf) return;
                    negotiationPanel.SetActive(true);
                    CurrentNegotiation currentNegotiation = GetCurrentNegotiation();
                    currentNegotiation.SetCurrentPrice(stand.GetCurrentPrice());
                    currentNegotiation.SetSeller(gameObject.GetComponent<SellerScript>());
                    negotiation = true;

            } else {
                // display message when seller doesnt want to negotiate
                GameInstance.Canvas.StartCoroutine(GameInstance.Canvas.ShowMessage("NoNegotiation"));
            }

        }

    }

   public IEnumerator StartSellerNegotiation()
    {
        CurrentNegotiation currentNegotiation = GetCurrentNegotiation();
        if (currentNegotiation == null) yield return null;
        bubbleCanvas.gameObject.SetActive(true);
        if (currentNegotiation.GetCurrentPrice() == stand.GetMinStandPrice())
        {
            StartCoroutine(ShowNegotiableMessage(stand.GetMinStandPrice()));
            // reached seller's minimum
            stand.SetNegotiablePrice(false);
            stand.SetCurrentPrice(stand.GetMinStandPrice());
        }
        else {
            // max - min
            int difference = stand.GetCurrentPrice() - stand.GetMinStandPrice();
            // how many negotiations
            int stepsCount = UnityEngine.Random.Range(2, 5);
            // how big discount for this negotiation
            int oneStep = difference / stepsCount;
            int sellerPrice = stand.GetCurrentPrice() - oneStep;
            int playerPrice = currentNegotiation.GetCurrentPrice();
            int priceToDisplay = sellerPrice;
            if ((sellerPrice - guessTolerance <= playerPrice) && (playerPrice <= sellerPrice + guessTolerance)) {
                priceToDisplay = playerPrice;
                if (playerPrice <= stand.GetMinStandPrice()) {
                    // cant go under the seller's minimum
                    priceToDisplay = stand.GetMinStandPrice();
                    stand.SetNegotiablePrice(false);
                } 
            }

            bubbleCanvas.gameObject.SetActive(true);
            StartCoroutine(ShowNegotiableMessage(priceToDisplay));

            stand.SetCurrentPrice(priceToDisplay);
        }


    }

    IEnumerator ShowNegotiableMessage(int priceToDisplay)
    {
        negotiablePrice.text = "";
        for (int i = 0; i <= fullText.Length; i++)
        {
            tempString = fullText.Substring(0, i);
            negotiableText.text = tempString;
            yield return new WaitForSeconds(delay);
        }
        negotiablePrice.text = priceToDisplay.ToString();
        yield return new WaitForSeconds(2f);
        bubbleCanvas.gameObject.SetActive(false);

    }

    private CurrentNegotiation GetCurrentNegotiation()
    {
        return GameObject.Find("CurrentNegotiaton").GetComponent<CurrentNegotiation>();

    }


    

}
