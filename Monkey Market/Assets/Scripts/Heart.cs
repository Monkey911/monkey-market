﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Heart : MonoBehaviour {

    public Heart heartPrefab;
    private Game GameInstance;

    private AudioSource AudioSource;
    private Heart NewHeart;
    private Queue<Heart> hearts = new Queue<Heart>();
    private bool BuffAlreadyGained = false;       

    private AudioClip AudioClip;

    // Use this for initialization
    void Start()
    {
        AudioSource = GetComponent<AudioSource>();
        GameInstance = Game.Instance;

        AudioClip = Resources.Load<AudioClip>("Sounds/heartBeating");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Spawn()
    {
        if (!GameInstance.GameOver)
        {
            NewHeart = Instantiate(heartPrefab);
            Vector3 newPosition = new Vector3(Random.Range(Game.MarketMinimumXValue, Game.MarketMaximumXValue),
                Random.Range(Game.MarketMinimumYValue, Game.MarketMaximumYValue), 0);

            while (!PositionManager.CheckIfPosEmpty(newPosition))
            {
                newPosition = new Vector3(Random.Range(Game.MarketMinimumXValue, Game.MarketMaximumXValue),
                    Random.Range(Game.MarketMinimumYValue, Game.MarketMaximumYValue), 0);
            }
            NewHeart.transform.position = newPosition;
            hearts.Enqueue(NewHeart);

        }
    }

    void OnMouseDown()
    {
        if (PositionManager.CheckIfPlayerNear(gameObject.transform.position) && !BuffAlreadyGained)
            HeartPickedUp();
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player" && !BuffAlreadyGained)
            HeartPickedUp();
    }

    private void HeartPickedUp()
    {
        GameInstance.IncreaseHealth(5);
        BuffAlreadyGained = true;
        hearts = new Queue<Heart>(hearts.Where(s => s != gameObject));
        AudioSource.PlayOneShot(AudioClip);
        Destroy(gameObject, AudioClip.length);
        gameObject.GetComponent<Renderer>().enabled = false;
    }

   

    public void Disappear()
    {
        Heart firstHeart = hearts.Dequeue();
        if (firstHeart != null && !GameInstance.GameOver && hearts.Count != 0)
        {
            Destroy(firstHeart.gameObject);
        }
    }

   
}
