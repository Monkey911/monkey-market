﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShoppingItemScript : MonoBehaviour {
   public Game GameInstance;
   public Stand stand;

    // Use this for initialization
    void Start () {
        GameInstance = Game.Instance;
    }

    // Update is called once per frame
    void Update () {
		
	}

    void OnMouseDown()
    {
        Debug.Log("click item " +  gameObject.tag);
        if (stand.GetMaxStandPrice() <= GameInstance.GetBudget() && PlayerManager.CheckIfPlayerNear(gameObject.transform.position))
        {
            // todo if the player goes away, e.g. buy it straight away, current price will be set to max again
            GameInstance.DecreaseBudget(stand.GetCurrentPrice());
            GameInstance.AddItem(stand.GetProductName());
            GameInstance.Canvas.CrossOutItem(stand.GetProductName());
            Destroy(gameObject);
        }
        else if (stand.GetMaxStandPrice() > GameInstance.GetBudget())
        {
                GameInstance.Canvas.StartCoroutine(GameInstance.Canvas.ShowMessage("Budget"));
    
        } else if (!PlayerManager.CheckIfPlayerNear(gameObject.transform.position)) {

            GameInstance.Canvas.StartCoroutine(GameInstance.Canvas.ShowMessage("Distance"));
           
        }
    }


}
