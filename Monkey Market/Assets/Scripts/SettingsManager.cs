﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour {
    public Toggle soundToogle;
    public Slider soundVolumeSlider;

    void OnEnable() {
        soundToogle.onValueChanged.AddListener(delegate { OnSoundToogle(); });
        soundVolumeSlider.onValueChanged.AddListener(delegate { OnSoundVolumeChange(); });
    }

    public void OnSoundToogle() {
        Debug.Log("isOn " + soundToogle.isOn);
        //gameInstance.soundOn = soundToogle.isOn;
    }

    public void OnSoundVolumeChange() {
        Debug.Log("volume " + soundVolumeSlider.value);
    }

    public void SaveSettings()
    {
        if (soundToogle.isOn)
        {
            AudioListener.volume = soundVolumeSlider.value;
        }
        else {
            AudioListener.volume = 0f;
        }
        SceneManager.LoadScene("LaunchScreen");
    }
}
