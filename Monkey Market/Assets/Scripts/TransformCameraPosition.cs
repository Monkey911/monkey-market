﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TransformCameraPosition : MonoBehaviour {

    public Camera Camera;
    private float minXPosition;
    private float maxXPosition;

	// Use this for initialization
	void Start () {
       
        minXPosition = (float) Game.Instance.GetCurrentLevelData()["camera_x_min"];
        maxXPosition = (float) Game.Instance.GetCurrentLevelData()["camera_x_max"];
    } 


    // Update is called once per frame
    void Update () {
        float x_value = gameObject.transform.position.x;
        if (x_value < minXPosition) x_value = minXPosition;
        else if (x_value > maxXPosition) x_value = maxXPosition;
        Camera.transform.position = new Vector3(x_value, Camera.transform.position.y, Camera.transform.position.z);
	}
}
