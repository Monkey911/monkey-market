﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollower : MonoBehaviour
{

    public Waypoint target;
  
    public float speed;
    private Animator CharacterAnimation;
    private SpriteRenderer CharacterRenderer;

    // Use this for initialization
    void Start ()
	{
        CharacterAnimation = GetComponent<Animator>();
        CharacterRenderer = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update ()
	{
	    if (target == null) return;

	    transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);


       CharacterAnimation.SetInteger("Direction", 1);


        if (Game.Instance.currentLevel == 1) { 
           if (GameObject.ReferenceEquals(target, GameObject.FindWithTag("EndPoint").GetComponent<Waypoint>())) {
                CharacterRenderer.flipX = false;
           
            }
       
            if (GameObject.ReferenceEquals(target, GameObject.FindWithTag("StartPoint").GetComponent<Waypoint>())) {
                CharacterRenderer.flipX = true;
            }

        }

        if (Vector3.Distance(transform.position, target.transform.position) <= float.Epsilon)
        {
            target = target.Next;
        }
    }


    private void SetAnimation(float x, float y)
    {

        if (x < 0.05 && x > -0.05 && y < 0.05 && y > -0.05)          // Character is idle 
            CharacterAnimation.SetInteger("Direction", 0);

        else if (y > 0.05)
        {
            CharacterAnimation.SetInteger("Direction", 3); // Moving up
            Debug.Log("moving up");
        }
        else if (Math.Abs(x) >= Math.Abs(y))
        {
            CharacterAnimation.SetInteger("Direction", 1);
            /*if (x > 0)                                                  // Moving right
                CharacterRenderer.flipX = false;
            else                                                        // Moving left
                CharacterRenderer.flipX = true;*/
        }

        else if (y < -0.05)
        {
            CharacterAnimation.SetInteger("Direction", 2); // Moving down
            Debug.Log("moving down");

            if (x > 0)
                CharacterRenderer.flipX = false;
            else
                CharacterRenderer.flipX = true;
        }
        else
            throw new InvalidOperationException("Uncovered x and y change in setting animation");
    }
}
