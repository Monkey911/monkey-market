﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CobraController : MonoBehaviour {

    public int harm;

    public float spawnTime = 3f;

    void Start () {
    }
	
	// Update is called once per frame
	void Update () {

        if (gameObject.transform.position.x > (float)Game.Instance.GetCurrentLevelData()["x_max"] && Game.Instance.currentLevel == 2)
        {
            Destroy(gameObject);
        }
    }

    public void SpawnCobra()
    {
        UnityEngine.Object objTemp = Resources.Load("Prefabs/Cobra");
        Instantiate(objTemp);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && Game.Instance.currentLevel > 1)
        {
            Game.Instance.DecreaseHealth(harm);
            Game.Instance.Canvas.StartCoroutine(Game.Instance.Canvas.ShowMessage("SnakeBite"));
        }
    }

}
