﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScript : MonoBehaviour {
    protected static CanvasScript instance;

    public Image content;
    public Text budgetValue;
    public Text healthValue;
    public Text item1, item2, item3, item4, item5, item6, item7, item8, item9, item10;
    public Text snakeBiteMessage;
    public Text carHitMessage;
    public Text noBudgetMessage;
    public Text distanceMessage;
    public Text noNegotiationMessage;
    public Text endGameTitle;
    public Text endGameSubtitle;
    public GameObject endGamePanel;
    public Button restartButton;
    public Button loadLevelButton;
    public Button togglePauseButton;
    public Sprite pauseImage;
    public Sprite playImage;

    private List<Text> textList = new List<Text>();
    void Start () {
        textList.Add(item1);
        textList.Add(item2);
        textList.Add(item3);
        textList.Add(item4);
        textList.Add(item5);

        if (Game.Instance.currentLevel > 1 && item6 != null && item7 != null) {
            textList.Add(item6);
            textList.Add(item7);

            if (Game.Instance.currentLevel == 3 && item8 != null && item9 != null && item10 != null)
            {
                textList.Add(item8);
                textList.Add(item9);
                textList.Add(item10);
            }
        }

    }
	
	void Update () {

	}

    public void UpdateProgressBar(float time) {
        content.fillAmount = Mathf.Lerp(0, 1, time);
    }

    public void UpdateBudgetValue(int newBudget) {
        budgetValue.text = newBudget.ToString();
    }

    public void UpdateHealthValue(int newHealth)
    {
        healthValue.text = newHealth.ToString();
    }

    public void CrossOutItem(string tag) {
        if (GameObject.FindWithTag(tag) != null) {
            // in shopping list
            Text itemText = GameObject.FindWithTag(tag).GetComponent<Text>() as Text;
            itemText.text = StrikeThrough(itemText.text);
            // do not cross out again
            itemText.tag = "BoughtItem";
        }

    }

    public string StrikeThrough(string s)
    {
        string strikethrough = "";
        foreach (char c in s)
        {
            strikethrough = strikethrough + c + '\u0336';
        }
        return strikethrough;
    }

    public void SetShoppingItems(List<ShoppingItem> items)
    {
        List<ShoppingItem>.Enumerator enumerator = items.GetEnumerator();

        for (int i = 0; i < textList.Count; i++)
        {
            bool hasNext = enumerator.MoveNext();
            if (!hasNext)
            {
                enumerator.Dispose();
                return;
            }
            textList[i].text = enumerator.Current.Description;
            textList[i].tag = enumerator.Current.Name;
        }
    }


    public IEnumerator ShowMessage(string typeMessage) {
        if (typeMessage.Equals("Budget"))
        {
            noBudgetMessage.gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
            noBudgetMessage.gameObject.SetActive(false);
        }
        else if (typeMessage.Equals("Distance"))
        {
            distanceMessage.gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
            distanceMessage.gameObject.SetActive(false);
        }
        else if (typeMessage.Equals("NoNegotiation")) {
            noNegotiationMessage.gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
            noNegotiationMessage.gameObject.SetActive(false);
        } else if (typeMessage.Equals("SnakeBite")) {
            snakeBiteMessage.gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
            snakeBiteMessage.gameObject.SetActive(false);

        } else if (typeMessage.Equals("CarHit"))
        {
            carHitMessage.gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
            carHitMessage.gameObject.SetActive(false);

        }


    }

    public void ShowEndGame(bool gameOver) {
        endGamePanel.SetActive(true);
        if (gameOver)
        {
            endGameTitle.text = "Game over!!";
            if (Game.Instance.GetHealth() <=0 && Game.Instance.currentLevel > 1)
            {
                endGameSubtitle.text = "You were transported to hospital.";

            } else {
                endGameSubtitle.text = "You didn't buy all items.";
            }

            endGameTitle.alignment = TextAnchor.MiddleCenter;

            restartButton.gameObject.SetActive(true);
            loadLevelButton.gameObject.SetActive(false);
        } else {
            restartButton.gameObject.SetActive(false);
            loadLevelButton.gameObject.SetActive(true);
        }

    }

    public void TogglePauseButton(bool isPaused)
    {
        if (isPaused)
        {
            togglePauseButton.GetComponent<Image>().sprite = playImage;
        } else {
            togglePauseButton.GetComponent<Image>().sprite = pauseImage;
        }
    }


}
