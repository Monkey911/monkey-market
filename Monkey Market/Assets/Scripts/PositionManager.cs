﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionManager : MonoBehaviour {

    public static bool CheckIfPlayerNear(Vector3 timerPosition)
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (Vector3.Distance(player.transform.position, timerPosition) <= Game.Instance.MinimumDistance)
        {
            return true;
        }

        return false;
    }

    public static bool CheckIfPosEmpty(Vector3 targetPos)
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player.transform.position == targetPos) return false;
        GameObject[] allMarketObjects = GameObject.FindGameObjectsWithTag("MarketObjects");
        foreach (GameObject current in allMarketObjects)
        {
            if (current.transform.position == targetPos)
                return false;
        }

        GameObject[] allCustomers = GameObject.FindGameObjectsWithTag("Customer");
        foreach (GameObject current in allCustomers)
        {
            if (current.transform.position == targetPos)
                return false;
        }
        return true;
    }
}
