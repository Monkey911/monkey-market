﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stand : MonoBehaviour {
    public int maxPrice;
    public int minPrice;
    public string productName;
    public bool negotiablePrice;
    public int currentPrice;

	// Use this for initialization
	void Start () {
        negotiablePrice = false;
        currentPrice = maxPrice;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int GetMaxStandPrice()
    {
        return maxPrice;
    }

    public int GetMinStandPrice()
    {
        return minPrice;
    }

    public int GetCurrentPrice()
    {
        return currentPrice;
    }

    public string GetProductName() {
        return productName;
    }

    public bool IsNegotiablePrice()
    {
        return negotiablePrice;
    }

    // the seller is willing to negotiate
    public void SetNegotiablePrice(bool negotiable) {
        negotiablePrice = negotiable;
    }

    public void SetMinStandPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public void SetCurrentPrice(int currentPrice)
    {
        this.currentPrice = currentPrice;
    }
}
