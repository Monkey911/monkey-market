﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    public InputField suggestedPrice;
    // display error message for bigger price than seller
    public Text errorMessage;

    // Use this for initialization
    void Start () {
        
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void StartGame() {
        SceneManager.LoadScene("Main");
    }

    public void ExitGame() {
        Application.Quit();
    }

    public void LoadNextLevel(string levelName) {
        SceneManager.LoadScene(levelName);
    }

    public void ShowOptionMenu() {
        SceneManager.LoadScene("OptionMenu");
    }

    public void ShowHelpMenu()
    {
        SceneManager.LoadScene("HelpScreen");
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void ClosePanel() {
        gameObject.SetActive(false);
    }

    public void DoSomeAction(bool isRestart, string level) {
        if (isRestart)
        {
            RestartGame();
        } else
        {
            LoadNextLevel(level);
        }
    }

    public void NegotiateAboutPrice()
    {
        GameObject currentPriceGO = GameObject.Find("CurrentNegotiaton");
        if (suggestedPrice == null || currentPriceGO == null) return;
        int currentStandPrice = currentPriceGO.GetComponent<CurrentNegotiation>().GetCurrentPrice();
        if (IsValidInput(suggestedPrice.text, currentStandPrice))
        {
            errorMessage.gameObject.SetActive(false);
            currentPriceGO.GetComponent<CurrentNegotiation>().SetCurrentPrice(Int32.Parse(suggestedPrice.text));
            SellerScript seller = currentPriceGO.GetComponent<CurrentNegotiation>().GetSeller();
            StartCoroutine(seller.StartSellerNegotiation());
            gameObject.SetActive(false);

            // clear input
            suggestedPrice.Select();
            suggestedPrice.text = "";
        }

        else
        {
            // wrong input
            errorMessage.gameObject.SetActive(true);
        }

    }

    private bool IsValidInput(string textToValid, int currentPrice)
    {
        int val = Int32.Parse(textToValid);
        // check if the player's price is not smaller
        return val < currentPrice;

    }

    public void GoToMainScreen() {
        SceneManager.LoadScene("LaunchScreen");
    }





}
