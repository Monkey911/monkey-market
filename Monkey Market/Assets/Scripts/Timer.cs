﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Timer : MonoBehaviour {
    private Game GameInstance;
    public Timer TimerPrefab;

    private AudioSource AudioSource;
    private Timer newTimer;
    private Queue<Timer> timers = new Queue<Timer>();
    private AudioClip AudioClip;
    private bool BuffAlreadyGained = false;


    // Use this for initialization
    void Start () {
        AudioSource = GetComponent<AudioSource>();
        GameInstance = Game.Instance;

        AudioClip = Resources.Load<AudioClip>("Sounds/timerSound");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Spawn() {
        if (!GameInstance.GameOver)
        {
            newTimer = Instantiate(TimerPrefab);
            Vector3 newPosition = new Vector3(Random.Range(Game.MarketMinimumXValue, Game.MarketMaximumXValue), 
                Random.Range(Game.MarketMinimumYValue, Game.MarketMaximumYValue), 0);

            while (!PositionManager.CheckIfPosEmpty(newPosition)) {
                newPosition = new Vector3(Random.Range(Game.MarketMinimumXValue, Game.MarketMaximumXValue), 
                    Random.Range(Game.MarketMinimumYValue, Game.MarketMaximumYValue), 0);
            }
            newTimer.transform.position = newPosition;
            timers.Enqueue(newTimer);
        }
    }

    void OnMouseDown()
    {
        if (PositionManager.CheckIfPlayerNear(gameObject.transform.position) && !BuffAlreadyGained)
            HourClassPickedUp();
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player" && !BuffAlreadyGained)
            HourClassPickedUp();
    }

    private void HourClassPickedUp()
    {
        GameInstance.DecreaseLeftTime(10);
        BuffAlreadyGained = true;
        timers = new Queue<Timer>(timers.Where(s => s != gameObject));
        AudioSource.PlayOneShot(AudioClip);
        Destroy(gameObject, AudioClip.length);
        gameObject.GetComponent<Renderer>().enabled = false;
    }


    public void Disappear() {
        if (timers.Count != 0) {
            Timer firstTimer = timers.Dequeue();
            if (firstTimer != null && !GameInstance.GameOver)
            {
                Destroy(firstTimer.gameObject);
            }
        }

    }

}
