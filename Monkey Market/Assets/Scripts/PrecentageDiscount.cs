﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PercentageDiscounts {
    // how big dicounts can be
    static readonly Dictionary<string, float> percentages = new Dictionary<string, float>
        {
            { "percentage10", 0.1f },
            { "percentage25", 0.25f },
            { "percentage50", 0.50f },
            { "percentage75", 0.75f }
        };

    public static float GetPercentageDiscount() { 
         return percentages.ElementAt(Random.Range(0, percentages.Count)).Value;
    }

}
